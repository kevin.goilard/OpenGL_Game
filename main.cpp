#include "OpenGLTools/device.h"
#include "OpenGLTools/Input/windowinput.h"
#include "OpenGLTools/Input/mouseinput.h"
#include "OpenGLTools/Input/keyboardinput.h"
#include "OpenGLTools/shaderrepository.h"
#include "OpenGLTools/program.h"
#include "OpenGLTools/glm.h"
#include "OpenGLTools/texturerepository.h"
#include "OpenGLTools\Model\modelrenderer.h"
#include "camera.h"
#include <iostream>
#include <GL/glew.h>

#include "Scene\SceneLoader.h"
#include "Scene\SceneDisplayer.h"

using namespace std;

int main(int argc, char *argv[])
{
	/* Initialisation du Contexte OpenGL en version 3.3 */
    Device device(800, 600, "OpenGL", 3, 3, true);

	// Ajout de la gestion des événements
    auto windowInput(std::make_shared<WindowInput>());
	auto mouseInput(std::make_shared<MouseInput>());
	auto keyboardInput(std::make_shared<KeyboardInput>());
    device.assignInput(windowInput);
	device.assignInput(mouseInput);
	device.assignInput(keyboardInput);

	// On cache le curseur et on l'emprisonne dans la fenêtre.
	device.hideCursor();

	// Classes permettant de gérer les shaders et programs
	ShaderRepository shaderRepository;

	// Classe permettant de charger les textures
	TextureRepository textureRepository;

	glEnable(GL_DEPTH_TEST); // On active le test de profondeur

	// On crée notre caméra
	CameraFPS camera(glm::vec3(1,1,1), 0.01f, 1.f, mouseInput, keyboardInput);

	SceneLoader sceneLoader;

	// On charge la scène dans le loader
	sceneLoader.load("../Models/map.png",
	{ std::make_pair(0, SceneBlock::MUR), // couleur noir représente un mur
		std::make_pair(0x505050, SceneBlock::DALLE),
		std::make_pair(0x606060, SceneBlock::DALLEUSED),
		std::make_pair(0xa0a0a0, SceneBlock::PORTE),
		std::make_pair(0xa1a1a1, SceneBlock::PORTEOPEN),
		std::make_pair(0xaaaaaa, SceneBlock::FIN),
		std::make_pair(0x0000ff, SceneBlock::SPEED),
		std::make_pair(0xffffff, SceneBlock::SOL) }, // Couleur blanche représente le sol
	1.f);

	SceneDisplayer displayer(shaderRepository,
	{ std::make_pair(SceneBlock::SOL, std::make_shared<ModelRenderer>("../Models/sol.obj", textureRepository)),
		std::make_pair(SceneBlock::SPEED, std::make_shared<ModelRenderer>("../Models/speed.obj", textureRepository)),
		std::make_pair(SceneBlock::DALLE, std::make_shared<ModelRenderer>("../Models/dalle.obj", textureRepository)),
		std::make_pair(SceneBlock::DALLEUSED, std::make_shared<ModelRenderer>("../Models/dalleused.obj", textureRepository)),
		std::make_pair(SceneBlock::PORTE, std::make_shared<ModelRenderer>("../Models/porte.obj", textureRepository)),
		std::make_pair(SceneBlock::PORTEOPEN, std::make_shared<ModelRenderer>("../Models/porteopen.obj", textureRepository)),
		std::make_pair(SceneBlock::FIN, std::make_shared<ModelRenderer>("../Models/fin.obj", textureRepository)),
	 std::make_pair(SceneBlock::MUR, std::make_shared<ModelRenderer>("../Models/mur.obj", textureRepository)) });

	auto scene = sceneLoader.load("../Models/map.png");

	scene.addSceneLinks(glm::vec2(1, 5), { glm::vec2(2,3) });//rouge
	scene.addSceneLinks(glm::vec2(9, 1), { glm::vec2(4,5) });//jaune
	scene.addSceneLinks(glm::vec2(17,5), { glm::vec2(11,2) });//bleu
	scene.addSceneLinks(glm::vec2(7,11), { glm::vec2(17,2),glm::vec2(15,6) });//vert
	scene.addSceneLinks(glm::vec2(15,11), { glm::vec2(1,10) });//marron
	scene.addSceneLinks(glm::vec2(9,13), { glm::vec2(15,17) });//rose

	

    while(windowInput->isRunning()) {
		if (!device.updateInputs())
			mouseInput->resetRelative();

		// Permet de quitter en appuyant sur la touche escape
		if (keyboardInput->key(SDL_SCANCODE_ESCAPE) == true)
			exit(0);

		// On update la caméra
		camera.update(&scene);

		// Efface l'écran et le depth buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// On l'affiche
		displayer.draw(scene, camera.view(), glm::perspective(glm::radians(70.f), (float)device.width() / device.height(), 0.01f, 500.f), camera.position() / scene.getScale());

        device.swapBuffers();
    }

    return 0;
}