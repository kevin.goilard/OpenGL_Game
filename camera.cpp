#include "camera.h"
// For Windows only
#ifdef _WIN32
#include <Windows.h>
#pragma comment(lib,"winmm.lib")
#endif // _WIN32


CameraFPS::CameraFPS(glm::vec3 const &pos,
                     float speed, float sensitivity,
                     std::shared_ptr<MouseInput> mouseInput,
                     std::shared_ptr<KeyboardInput> keyboardInput) :
    mPos(pos), mLook(0.0, 0.0, 0.0), mUp(glm::vec3(0.0, 1.0, 0.0)),
    mMouseInput(mouseInput),
    mKeyboardInput(keyboardInput),
    mPhi(0.0), mTheta(-2.0),
    mSpeed(speed), mSensitivity(sensitivity)
{
	// Z, S, Q, D : Qwerty way
    mKeyMap[FORWARD] = SDL_SCANCODE_W;
    mKeyMap[BACKWARD] = SDL_SCANCODE_S;
    mKeyMap[LEFT] = SDL_SCANCODE_A;
    mKeyMap[RIGHT] = SDL_SCANCODE_D;
	finished = false;
}

void CameraFPS::mComputeForwardVector(void)
{
	// Calcul du vecteur de direction en fonction des coordonnées sphériques
    float phiRad = mPhi * M_PI / 180.0f;
    float thetaRad = mTheta * M_PI / 180.0f;

    float cPhi = cos(phiRad);
    float cTheta = cos(thetaRad);
    float sPhi = sin(phiRad);
    float sTheta = sin(thetaRad);

	// Permet de bloquer les mouvements de camera dans le plan
	mForward.y = sPhi;
	mForward.z = cPhi * cTheta;
    mForward.x = cPhi * sTheta;
}

void CameraFPS::update(void)
{
    // Mouse
    mPhi   -= mMouseInput->yRel() * mSensitivity;
    mTheta -= mMouseInput->xRel() * mSensitivity;

    if(mPhi > 89.0f)
        mPhi = 89.0f;

    else if(mPhi < -89.0f)
        mPhi = -89.0f;

    if(mTheta > 360.0f)
        mTheta = 0.0f;

    else if(mTheta < -360.0f)
        mTheta = 0.0f;

    mComputeForwardVector();

    // KeyBoard

	// Utilisation de vec3 temporaire pour bouger uniquement dans le plan
	glm::vec3 toAddForward = mForward;
	toAddForward.y = 0;
	glm::vec3 toAddLeft = mLeft;
	toAddLeft.y = 0;

    if(mKeyboardInput->key(mKeyMap[FORWARD]) == true)
        mPos += glm::vec3(toAddForward * mSpeed);

    if(mKeyboardInput->key(mKeyMap[BACKWARD]) == true)
        mPos -= glm::vec3(toAddForward * mSpeed);

    if(mKeyboardInput->key(mKeyMap[LEFT]) == true)
        mPos += glm::vec3(toAddLeft * mSpeed);

    if(mKeyboardInput->key(mKeyMap[RIGHT]) == true)
        mPos -= glm::vec3(toAddLeft * mSpeed);

    mLeft = glm::normalize(cross(mUp, mForward));

    mLook = mPos.xyz() + mForward;
}

void CameraFPS::update(Scene* sc)
{
	// Mouse
	if(mMouseInput->yRel() < 1000 && mMouseInput->yRel() > -1000)
	mPhi -= mMouseInput->yRel() * mSensitivity;
	if(mMouseInput->xRel() <1000 && mMouseInput->xRel() > -1000)
	mTheta -= mMouseInput->xRel() * mSensitivity;

	if (mPhi > 89.0f)
		mPhi = 89.0f;

	else if (mPhi < -89.0f)
		mPhi = -89.0f;

	if (mTheta > 360.0f)
		mTheta = 0.0f;

	else if (mTheta < -360.0f)
		mTheta = 0.0f;

	mComputeForwardVector();

	// KeyBoard

	// Utilisation de vec3 temporaire pour bouger uniquement dans le plan
	glm::vec3 toAddForward = mForward;
	toAddForward.y = 0;
	glm::vec3 toAddLeft = mLeft;
	toAddLeft.y = 0;

	glm::vec3 mPosTmp = mPos;
	glm::vec3 frontMove;
	glm::vec3 leftMove;
	glm::vec3 frontBlockTmp;
	glm::vec3 leftBlockTmp;

	if (mKeyboardInput->key(mKeyMap[FORWARD]) == true)
	{
		frontMove= glm::vec3(toAddForward * mSpeed);
		frontBlockTmp = mPosTmp + frontMove + mForward/2.f;
	}
	else if (mKeyboardInput->key(mKeyMap[BACKWARD]) == true)
	{
		frontMove = -glm::vec3(toAddForward * mSpeed);
		frontBlockTmp = mPosTmp + frontMove - mForward/2.f;
	}
	if (mKeyboardInput->key(mKeyMap[LEFT]) == true)
	{
		leftMove = glm::vec3(toAddLeft * mSpeed);
		leftBlockTmp = mPosTmp + leftMove + mLeft/2.f;
	}
	else if (mKeyboardInput->key(mKeyMap[RIGHT]) == true)
	{
		leftMove = -glm::vec3(toAddLeft * mSpeed);
		leftBlockTmp = mPosTmp + leftMove - mLeft/2.f;
	}
	if (sc->get(frontBlockTmp) != SceneBlock::MUR && sc->get(leftBlockTmp) != SceneBlock::MUR && sc->get(frontBlockTmp) != SceneBlock::PORTE && sc->get(leftBlockTmp) != SceneBlock::PORTE)
		mPosTmp += frontMove + leftMove;
	else if (sc->get(frontBlockTmp) != SceneBlock::MUR && sc->get(frontBlockTmp) != SceneBlock::PORTE)
		mPosTmp += frontMove;
	else if (sc->get(leftBlockTmp) != SceneBlock::MUR && sc->get(leftBlockTmp) != SceneBlock::PORTE)
		mPosTmp += leftMove;
	if(!finished)
		mPos = mPosTmp;
	if (sc->get(mPos) == SceneBlock::SPEED)
	{
		mSpeed *= 1.5;
		sc->set(mPos, SceneBlock::SOL);
	}
	if (sc->get(mPos) == SceneBlock::DALLE)
	{
		//For Windows only
#ifdef _WIN32
		PlaySound(TEXT("../Sounds/pressure.wav"), NULL, SND_FILENAME | SND_ASYNC);
#endif // _WIN32

		sc->set(mPos, SceneBlock::DALLEUSED);
		glm::vec3 posInArray = mPos / sc->getScale();

		int x = roundf(posInArray.x);
		int z = roundf(posInArray.z);
		
		sc->activatePressure(x,z);
	}
	if (sc->get(mPos) == SceneBlock::FIN)
		finished = true;
	mLeft = glm::normalize(cross(mUp, mForward));

	mLook = mPos.xyz() + mForward;
}
