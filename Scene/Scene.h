#pragma once
#include <vector>
#include "../OpenGLTools/glm.h"

enum class SceneBlock {SOL, MUR, DALLE, DALLEUSED, PORTE, PORTEOPEN, SPEED, FIN, UNDEFINED};

class Scene {
public:
	Scene() = default;
	Scene(std::vector<std::vector<SceneBlock>> scene, float scale);

	SceneBlock get(glm::vec3 const &position) const;
	void set(int x, int z,SceneBlock block);
	void set(glm::vec3 const & position, SceneBlock block);
	SceneBlock get(int x, int z) const;

	int getWidth() const;
	int getHeight() const;

	float getScale() const;

	std::vector < std::pair<glm::vec2, std::vector<glm::vec2>>> links;
	void addSceneLinks(glm::vec2 pressure, std::vector<glm::vec2> doors);
	void activatePressure(int x, int z);

private:
	std::vector<std::vector<SceneBlock>> mBlocks;
	float mScale;
	int mWidth;
	int mHeight;
};