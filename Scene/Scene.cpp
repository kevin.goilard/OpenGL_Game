#include <utility>
#include "Scene.h"

Scene::Scene(std::vector<std::vector<SceneBlock>> scene, float scale) :
	mBlocks(std::move(scene)),
	mScale(scale),
	mWidth(mBlocks[0].size()),
	mHeight(mBlocks.size()) {

	for (auto line : mBlocks)
		assert(line.size() == mBlocks[0].size());
}

SceneBlock Scene::get(int x, int z) const {
	if (x >= 0 && z >= 0 && x < mWidth && z < mHeight)
		return mBlocks[z][x];
	else
		return SceneBlock::UNDEFINED;
}

SceneBlock Scene::get(glm::vec3 const &position) const {
	glm::vec3 posInArray = position / mScale;

	int x = roundf(posInArray.x);
	int z = roundf(posInArray.z);

	return get(x, z);
}

void Scene::set(int x, int z,SceneBlock block) {
	if (x >= 0 && z >= 0 && x < mWidth && z < mHeight)
		mBlocks[z][x] = block;
}

void Scene::set(glm::vec3 const &position, SceneBlock block) {
	glm::vec3 posInArray = position / mScale;

	int x = roundf(posInArray.x);
	int z = roundf(posInArray.z);

	set(x, z,block);
}

int Scene::getWidth() const {
	return mWidth;
}

int Scene::getHeight() const {
	return mHeight;
}

float Scene::getScale() const {
	return mScale;
}

void Scene::addSceneLinks(glm::vec2 pressure, std::vector<glm::vec2> doors)
{
	links.push_back(std::make_pair(pressure, doors));
}

void Scene::activatePressure(int x, int z)
{
	for (int i = 0; i < (links.size()); ++i)
	{
		if (links[i].first.x == x && links[i].first.y == z)
		{
			for (int j = 0; j < (links[i].second.size()); ++j)
				set(links[i].second[j].x, links[i].second[j].y, SceneBlock::PORTEOPEN);
		}
	}
}
