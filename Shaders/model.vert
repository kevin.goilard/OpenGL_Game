#version 330

// Données entrantes depuis le VAO
layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTextureCoordinate;

// Données sortantes vers le Fragment Shader
out vec2 textureCoordinate;
out float intensity;

// Définit le type de matrices
#define WORLD 0
#define VIEW 1
#define PROJ 2

// Tableau des 3 matrices
uniform mat4 matrices[3];
uniform vec3 light;

void main(void) {
    gl_Position = matrices[PROJ] * matrices[VIEW] * matrices[WORLD] * vec4(inPos, 1);
    textureCoordinate = inTextureCoordinate;
	vec3 tmpPos = (matrices[WORLD] * vec4(inPos,1)).xyz;
	vec3 posToLight = light - tmpPos;
	float brightness = 1.0f/length(posToLight);
	intensity =clamp(brightness,0.1f,1.0f);
}
